# projetoBD

Trabalho da disciplina Bancos de Dados 2017/2 com a professora Maristela Holanda UnB.

# Introdução

O Trabalho consiste em extrair dados do governo para gerar consultas que respondam a perguntas interessantes e exibir numa interface amigável. Desse modo iremos aplicar vários conceitos da disciplina Bancos de Dados, como Diagrama Entidade Relacionamento, Normalização e SQL.

Este trabalho irá fazer *Queries* que respondem as seguintes perguntar:
- Quantas escolas possuem mais de 50 funcionários?
- Quantas turmas dão aula 5 dias ou mais na semana?
- Quantas turmas dão aula sábado ou domingo?
- Como está distribuída a escolarização secundária no Brasil?
- Como está distribuído os docentes por área urbana e rural?
- Quantas escolas possuem acesso a internet?

#### Integrantes do Grupo
Aluno | Matrícula | Usuário
------|-----------|--------
André Filipe Caldas Laranjeira | 16/0023777 | @AndreLaranjeira
Diogo Queiroz dos Santos | 15/0123418 | @diogoqds
Victor André Gris Costa | 16/0019311 | @victoragcosta
Vitor Ribeiro Custódio | 15/0023502 | @VitorRibeiroCustodio

Todo versionamento do programa se encontra no github do projeto aqui https://github.com/diogoqds/projetoBD

# Diagrama Entidade Relacionamento (DER)

![Diagrama Entidade Relacionamento](./Imagens/modelo_DER.jpg)

Relacionamentos:

1. Tipo de localização da escola.
2. Situação de funcionamento da escola.
3. Categoria privada da escola.
4. Tipo de dependência administrativa da escola.
5. Região da UF.
6. UF da escola.
7. Município da escola.
8. Sexo do docente.
9. Cor/Raça do docente.
10. Nacionalidade do docente.
11. País de origem do docente.
12. UF de nascimento do docente.
13. UF de residência do docente.
14. Município de nascimento do docente.
15. Município de residência do docente.
16. Situação funcional do docente.
17. Escolaridade do docente.
18. Funções escolares dos docentes.
19. Escolas dos docentes.
20. Turmas das escolas.
21. Mediação didático-pedagógica da turma.
22. Etapa de ensino da turma.
23. Turmas dos docentes.
24. Alunos das turmas.
25. Sexo do aluno.
26. Cor/Raça do aluno.
27. País de origem do aluno.
28. Nacionalidade do aluno.
29. Etapa de ensino do aluno.
30. Escolarização secundária do aluno.
31. Escola do aluno.
32. Município de nascimento do aluno.
33. Município de residência do aluno.
34. UF de nascimento do aluno.
35. UF de residência do aluno.

# Modelo Relacional (MR)

![Modelo Relacional](./Imagens/diagrama_MR.png)

# Tabelas de dados originais

Os dados estavam concentrados em 4 tabelas com múltiplas colunas. As tabelas de TURMAS.CSV e ESCOLAS.CSV vieram em um único arquivo para cada tabela. As outras duas, MATRICULAS_\*\*.CSV e DOCENTES_\*\*.CSV vieram em 5 arquivos cada, um para cada região do Brasil, Centro Oeste (CO), Norte, Nordeste, Sudeste, Sul. As tabelas originais estavam somente na primeira forma normal e continha muitos dados com múltiplos valores. Por exemplo as coluna com o CPF do docente e a coluna de id da turma geravam múltiplos docentes iguais.

# Processo de Normalização

### Primeira forma normal (1FN)

Para que um conjunto de tabelas esteja na 1FN, não podem haver atributos multivalorados nas tabelas. Ou seja, as colunas das tabelas devem ser indivisíveis.

As tabelas de dados iniciais já estavam na 1FN, pois não possuiam atributos multivalorados.

### Segunda forma normal (2FN)

Para que um conjunto de tabelas esteja na 2FN, é preciso que essas tabelas já estejam na 1FN e que cada atributo de uma tabela seja totalmente funcionalmente dependente da(s) chave(s) dessa tabela. Isso implica em duas coisas. Primeiro, para cada chave contida em uma tabela, só existe um valor possível para cada atributo contido nesta mesma tabela. Segundo, todas as chaves contidas em uma tabela são necessárias para se determinar unicamente os atributos dessa tabela. Ou seja, não se pode utilizar menos chaves em uma dada tabela para se chegar a um valor único para os atributos dessa tabela.

Algumas das tabelas de dados iniciais não estavam na 2FN. Abaixo são listadas quais tabelas continham dados que não podiam ser totalmente funcionalmente determinados pelas chaves das tabelas.

1. Docentes: **[PK]** idDocentes ¬-> **[Atributos]** idEscola, idTurma, idFuncaoEscolar
2. Aluno: **[PK]** idMatricula ¬-> **[Atributos]** idTurma

Percebeu-se que os atributos em questão deveriam ser tratados como chaves, e não atributos. Assim, esses atributos foram retirados das tabelas originais e novas tabelas foram criadas para abrigar esses atributos (agora como chaves), de forma a manter a 2FN. O resultado final foi o seguinte conjunto de operações:

1. *(Remoção de atributos)* Docentes: **[Atributos removidos]** idEscola, idTurma, idFuncaoEscolar
2. *(Remoção de atributos)* Aluno: **[Atributo removido]** idTurma
3. *(Nova tabela)* EscolasDosDocentes: **[PK]** (idDocente, idEscola)
4. *(Nova tabela)* TurmasDosDocentes: **[PK]** (idDocente, idTurma)
5. *(Nova tabela)* FuncoesDosDocentes: **[PK]** (idDocente, idFuncaoEscolar)
6. *(Nova tabela)* TurmasDosAlunos: **[PK]** (idMatricula, idTurma)

Com isso, as tabelas de dados passaram a estar na 2FN.

### Terceira forma normal (3FN):

Para que um conjunto de tabelas esteja na 3FN, é preciso que essas tabelas já estejam na 2FN e que todos os atributos não-chave de cada tabela sejam dependentes não-transitivos das chaves dessa tabela. Ou seja, os atributos não-chave de cada tabela devem ser totalmente funcionalmente dependentes apenas da chave primária dessa tabela. Caso algum atributo seja totalmente funcionalmente dependente de um outro atributo não-chave dessa tabela, a tabela antiga deve ser modificada e uma nova tabela deve ser criada.

As tabelas de dados iniciais já estavam na 3FN conforme os dados presentes em cada uma. Entretanto, percebemos que um par de dados possivelmente violava essa regra:

1. Escola: **[PK]** idEscola -> **[Atributos]** idRegiao, idUF

Sabemos que cada unidade federativa (UF) pertence exclusivamente a uma região geográfica. Portanto, é lógico supor que: (idUF -> idRegião), o que violaria a 3FN. Entretanto, não encontramos, na tabela de dados UnidadeFederativa fornecida, o atributo idRegiao. Assim, decidimos implementar esses dados manualmente, tendo-se em vista que o Brasil possui apenas 27 UFs e 5 regiões geográficas (Uma quantidade pequena de dados). O resultado final foi o seguinte conjunto de operações:

1. *(Adição de atributos)* UnidadeFederativa: **[Atributo adicionado]** idRegiao
2. *(Remoção de atributos)* Escola: **[Atributo removido]** idRegiao

### Conclusão:

Com essas modificações, os dados do conjunto de tabelas dos dados originais foram normalizados, e, assim, o modelo relacional (MR) pode ser validado.

# Script SQL Gerador

O script necessário para gerar o banco de dados está disponível no arquivo [**Script.sql**](./Script.sql).

# Processo ETL (*Extract*, *Transfer*, *Load*)

O processo ETL foi meio variado quanto aos métodos. De primeira os dados foram trabalhados no RStudio, scripts Ruby e no LibreOffice. Os arquivos eram abertos (com muita dificuldade) e eram observadas as colunas que se desejavam retirar para nosso MR previamente feito. Assim foi gerado um arquivo SQL com as tabelas menores (menos de 10000 entradas). Nesse mesmo método foram criados arquivos.csv para as tabelas de maior quantidade de dados. Escolheu-se trabalhar com 10000 linhas para os arquivos gerados, porque estava basicamente impossível lidar com mais dados que isso. A partir desses .csv nós importamos os dados pelo MySQL Workbench. Após complicações geramos arquivos SQL também, porém não estávamos satisfeitos com a quantidade de dados efetivamente entrando no banco de dados.

Depois desse problema com a quantidade de dados que gerou poucas linhas efetivamente no banco de dados, utilizamos scripts python feitos por nós mesmos.

O script [mostra_colunas.py](./ETL/mostra_colunas.py) foi utilizado para mostrar cada coluna existente no arquivo, já que abrir em softwares como Microsoft Excel ou LibreOffice estava praticamente impossível com um tempo de carregamento enorme e consumo de memória RAM enorme também. Esse script se fez necessário pois encontramos um conjunto de colunas na ordem diferente da do LEIA-ME.pdf disponibilizado junto dos dados.

Os scripts SQL_gerador_(tabela).py, que estão disponibilzados na pasta [ETL](./ETL) serviram para gerar arquivos de 250000 linhas cada contendo Inserts para popular nossa tabela. Essa divisão em arquivos de 250000 linhas foi importante para permitir uma inserção parcial de algumas tabelas mais bem distribuidas pelas regiões. Devo ressaltar que a quantidade de dados sobre alunos é impressionantemente enorme, tendo apenas o Sudeste 21 milhões de linhas sobre alunos e com poucas repetições de aluno. Por esse motivo optamos por usar apenas 2 milhões de alunos distribuídos igualmente por todas as regiões.
A partir desses .sql gerados nós criamos um script que usa um conector mysql para python que faria esse trabalho de inserir para nós. Esse script foi automatizado para inserir todos os inserts dos arquivos em uma ordem com as tabelas dependentes de outras por último. O script também inseria de uma forma que distribuia bem a quantidade de alunos por região igualmente. As pastas com os arquivos devem ser geradas em cada computador por ser inviável o upload para o github. Os microdados do censo de 2015 foram deixados de fora do trabalho, já que se pode obter facilmente e também é enorme, no entanto a pasta deveria estar extraída com todas as tabelas dentro dele extraídas para todos os scripts funcionarem corretamente.


Ao fim nós utilizamos o banco de dados com os dados e a estrutura prontas para gerarem scripts do framework Rails para Ruby que gerariam a tabela. Fizemos isso por praticidade para outros criarem a database dentro do próprio PC. As grandes tabelas continuaram sendo preenchidas com os scripts em python por conta da inviabilidade de gerar um seed para elas. Ao fim temos mais ou menos 2,5 milhões de alunos, 2 milhões de docentes, 2 milhões de turmas e 272000 turmas, inserindo completamente as tabelas de docentes, turmas e escolas. Novamente, a quantidade de alunos inviabilizou a população completa sem quebrar nossos computadores.

# Procedure

Para chamar o procedure use `CALL delete_backup();` que tem a função de deletar os dados da tabela BackupEscola se houver mais de 100 dados
```
CREATE PROCEDURE delete_backup ()
BEGIN
  IF((SELECT COUNT(*) FROM BackupEscola) > 100) then
  DELETE FROM BackupEscola;
  END IF;
END $
```

# Triggers

Os Triggers geram um backup da tabela docente e escola quando uma linha é deletada e há mais do que 1000 linhas

```
CREATE TRIGGER `trg_escola`
BEFORE DELETE ON `Escola`
FOR EACH ROW
BEGIN
  IF((SELECT COUNT(*) FROM Escola) > 1000 ) then
  INSERT INTO BackupEscola VALUES(
    OLD.idEscola,
    OLD.DataNascDocente,
    OLD.idSitEscola,
    OLD.InicioAnoLetivo,
    OLD.FimAnoLetivo,
    OLD.id_UF,
    OLD.idMunicipio,
    OLD.idDependenciaAdm,
    OLD.idTipoLocalizacao,
    OLD.idCatPrivada,
    OLD.CodIES,
    OLD.TotalFuncionarios,
    OLD.Internet,
    OLD.Compartilhada);
    END IF;
END
$
```

```
CREATE TRIGGER `trg_docentes`
BEFORE DELETE ON `Docente`
FOR EACH ROW
BEGIN
  IF((SELECT COUNT(*) FROM Docente) > 1000 ) then
  INSERT INTO BackupEscola VALUES(
    OLD.idDocentes,
    OLD.NomeEscola,
    OLD.idSexo,
    OLD.idCor_Raca,
    OLD.idNacionalidade,
    OLD.idPaisOrigem,
    OLD.idMunicipioNascimento,
    OLD.idMunicipioResidencia,
    OLD.idUFNascimento,
    OLD.idUFResidencia,
    OLD.Deficiencia,
    OLD.Def_Cegueira,
    OLD.Def_BaixaVisao,
    OLD.Def_Surdez,
    OLD.Def_DefAuditiva,
    OLD.Def_SurdoCegueira,
    OLD.Def_DefFisica,
    OLD.Def_DefIntelectual,
    OLD.Def_DefMultipla,
    OLD.idEscolaridade,
    OLD.idSituacaoFunciona);
   END IF;
END
$
```

# Views

Para acessar a view use `SELECT * FROM <Nome da View>;`

```
CREATE VIEW  `Censo_escolar_2015`.`ViewEscola` AS
SELECT C.idEscola,
        C.NomeEscola,
                S.idSitEscola,
                C.InicioAnoLetivo,
                C.FimAnoLetivo,
                U.id_UF,
                M.idMunicipio,
                D.idDependenciaAdm,
                T.idTipoLocalizacao,
                P.idCatPrivada,
                C.CodIES,
                C.TotalFuncionarios,
                C.Internet,
                C.Compartilhada
FROM Escola C
INNER JOIN SituacaoEscola S
INNER JOIN UnidadeFederativa U
INNER JOIN Municipio M
INNER JOIN idDependenciaAdm D
INNER JOIN idTipoLocalizacao T
INNER JOIN idCatPrivada P;
```

```
CREATE VIEW  `Censo_escolar_2015`.`ViewSituacaoEscola` AS
SELECT C.idSitEscola,
        C.NomeSitEscola
FROM SituacaoEscola C;
```

```
CREATE VIEW  `Censo_escolar_2015`.`ViewRegiao` AS
SELECT C.idRegiao,
        C.NomeRegiaoGeo
FROM Regiao C;
```

```
CREATE VIEW  `Censo_escolar_2015`.`ViewDocente` AS
SELECT C.idDocente,
        C.DataNascDocente,
        C.idSexo,
        C.idCor_Raca,
        C.idNacionalidade,
        C.idPaisOrigem,
        C.idMunicipioNascimento,
        C.idMunicipioResidencia,
        C.idUFNascimento,
        C.idUFResidencia,
        C.Deficiencia,
        C.Def_Cegueira,
        C.Def_BaixaVisao,
        C.Def_Surdez,
        C.Def_DefAuditiva,
        C.Def_SurdoCegueira,
        C.Def_DefFisica,
        C.Def_DefIntelectual,
        C.Def_DefMultipla,
        C.idEscolaridade,
        C.idSituacaoFuncional
FROM Docente C;
```

```
CREATE VIEW  `Censo_escolar_2015`.`ViewTurma` AS
SELECT C.idTurma,
        C.idEscola,
        C.NomeTurma,
        C.HoraInicial,
        C.DuracaoMin,
        C.NumMatriculas,
        C.idMediacaoDidPed,
        C.ClasseEspecial,
        C.idEtapaEnsino,
        C.DiasPorSemana,
        C.Dia_Domingo,
        C.Dia_Segunda,
        C.Dia_Terça,
        C.Dia_Quarta,
        C.Dia_Quinta,
        C.Dia_Sexta,
        C.Dia_Sabado
FROM Turma C;
```

```
CREATE VIEW  `Censo_escolar_2015`.`ViewAluno` AS
SELECT C.idMatricula,
        C.CodAluno,
        C.idEscola,
        C.DataNascAluno,
        C.idSexo,
        C.idCor_Raca,
        C.idNacionalidade,
        C.idPaisOrigem,
        C.idMunicipioNascimento,
        C.idMunicipioResidencia,
        C.idUFNascimento,
        C.idUFResidencia,
        C.idEtapaEnsino,
        C.idEscolarizacaoSec
FROM Aluno C;
```

```
CREATE VIEW  `Censo_escolar_2015`.`ViewBackupEscola` AS
SELECT C.idBackupEscola,
        C.NomeEscola,
                C.idSitEscola,
                C.InicioAnoLetivo,
                C.FimAnoLetivo,
                C.id_UF,
                C.idMunicipio,
                C.idDependenciaAdm,
                C.idTipoLocalizacao,
                C.idCatPrivada,
                C.CodIES,
                C.TotalFuncionarios,
                C.Internet,
                C.Compartilhada
FROM BackupEscola C;
```

```
CREATE VIEW  `Censo_escolar_2015`.`ViewBackupDocentes` AS
SELECT C.idBackupDocentes,
        C.DataNascDocente,
        C.idSexo,
        C.idCor_Raca,
        C.idNacionalidade,
        C.idPaisOrigem,
        C.idMunicipioNascimento,
        C.idMunicipioResidencia,
        C.idUFNascimento,
        C.idUFResidencia,
        C.Deficiencia,
        C.Def_Cegueira,
        C.Def_BaixaVisao,
        C.Def_Surdez,
        C.Def_DefAuditiva,
        C.Def_SurdoCegueira,
        C.Def_DefFisica,
        C.Def_DefIntelectual,
        C.Def_DefMultipla,
        C.idEscolaridade,
        C.idSituacaoFuncional
FROM BackupDocentes C;
```

---
# Configuração inicial

* Instale Ruby (versão 2.4, recomendo o [RVM](https://rvm.io/rvm/install) para gerenciar as versões de Ruby, existe também o [rbenv](https://github.com/rbenv/rbenv)). Recomendo seguir o [tutorial da Digital Ocean para instalar RVM](https://www.digitalocean.com/community/tutorials/how-to-install-ruby-on-rails-with-rvm-on-ubuntu-16-04)

* Instale a gem de rails `gem install rails` na última versão (5.1.4)

* Instale mysql

* Abra ~/.bashrc com o seu editor de texto preferido e adicione as linhas
`export DB_USERNAME_MYSQL="<nome de usuário do mysql>"`,
`export DB_PASSWORD_MYSQL="<senha do mysql>"` e
`export DB_HOST_MYSQL="<endereço do servidor>"`(Caso não tenha, o default é `localhost`)
e reinicie o bash

* execute `rake db:create` e `rake db:schema:load` na pasta Programa/projetoBD, se der erro, houve problema na configuração do banco de dados

* execute `SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));` no mysql para desabilitar esse modo do capeta

* Teste acessando a pasta do Programa/projetoBD rodando `rails s` e abrindo `localhost:3000`. Se exibir uma página como a abaixo, o projeto está configurado. Para fechar o servidor aperte `Ctrl+C`.

![Exemplo Linha de comando](./Imagens/readme_exemplo2.jpg)

![Exemplo de página](./Imagens/readme_exemplo.jpg)

---

### **A FAZER**

- [x] Escrever Introdução;
- [x] DER;
- [x] MR;
- [x] Tabelas de dados originais;
- [x] Forma Normal (mínimo 3 tabelas);
- [x] Script SQL gerador do BD (MR);
- [x] Processo ETL;
- [x] Camada de persistência (mínimo de 5 consultas);
- [x] Views no SGBD;
- [x] 1 Procedure (com comandos condicionais);
- [x] 1 Trigger (com comandos condicionais);
- [x] Escrever instruções de projeto.

---

### Ferramentas e Links Úteis

- [Converter ActiveRecord para SQL](https://apidock.com/rails/ActiveRecord/Relation/to_sql)
- [Criar Models a partir de um SQL gerador](https://stackoverflow.com/questions/41381788/rails-how-to-automatically-create-models-from-an-sql-file)
- [Documentação Materialize](http://materializecss.com/getting-started.html)
- [Materialize gem](https://github.com/mkhairi/materialize-sass)
- [Documentação Semantic-ui](https://semantic-ui.com/collections/message.html)
- [Semantic-ui gem](https://github.com/doabit/semantic-ui-sass)
- [ngrok (exibir para mostrar aplicação)](https://ngrok.com/)
- [Preparar Sublime para usar com Rails](https://mattbrictson.com/sublime-text-3-recommendations)
- [Começando com Rails](http://guides.rubyonrails.org/getting_started.html)
