from os import system, listdir
import mysql.connector
from time import time

VERBOSE = False
frequency = 10000

directories = ['docente_inserts',
'aluno_inserts',
'turmasdosalunos_inserts',
'funcoesdosdocentes_inserts',
'turmasdosdocentes_inserts',
'escolasdosdocentes_inserts']

for directory in directories:
  inseridos = '{0}/inseridos.txt'.format(directory)

  arq1 = open(inseridos, 'r')
  done = []
  for line in arq1:
    done.append(line.replace('\n',''))
  arq1.close()

  try:
    cnx = mysql.connector.connect(user='root',
                                  database='projetobd_development',
                                  password='Euamogarrysmod98',
                                  host='127.0.0.1')

    files = listdir(directory)
    files.sort(key=lambda file: file[-6:-4])
    for filename in files:

      found = False
      for inserted in done:
        if filename.find(inserted) != -1:
          found = True

      if not found:
        print('inserting:',filename)
        erros = 0
        acertos = 0
        linhas = 0
        comeco = time()
        try:
          cursor = cnx.cursor()
          arq2 = open(directory+'/'+filename, 'r')
          parcial_comeco = time()
          for sql in arq2:
            try:
              cursor.execute(sql)
            except mysql.connector.errors.IntegrityError as err:
              erros += 1
              linhas += 1
              if VERBOSE:
                print(err)
            else:
              acertos += 1
              linhas += 1
            finally:
              if linhas >= frequency or (time()-parcial_comeco) >= 10:
                print('\rErros:',erros,
                '| Acertos:', acertos,
                '| Total:', erros + acertos,
                '| Tempo:', time()-parcial_comeco, 's',
                end = '')
                linhas = 0
                parcial_comeco = time()
                cnx.commit()
          print()
        except mysql.connector.errors.IntegrityError as err:
          print('Erro na inserção:', err)
        except KeyboardInterrupt:
          continuar = input('Deseja continuar a inserção?(s/n):')
          if continuar.lower() == 's':
            print('Ok, vou pular {0} então'.format(filename))
          else:
            print('Ok, vou terminar o programa para você.')
            break
        else:
          arq1 = open(inseridos, 'a')
          arq1.write(filename+'\n')
          arq1.close()
        finally:
          cnx.commit()
          cursor.close()
          arq2.close()
        fim = time()
        print('''Finalizado.
          {0} inserções feitas de {1} em {3:.3f} segundos.
          Porcentagem de inserção:{2:.2f}%
          Tempo médio por insert: {4}'''
          .format(acertos, erros+acertos, 100*acertos/(erros+acertos), fim-comeco, (fim-comeco)/(erros+acertos)))
      else:
        print('jumping:',filename)

  finally:
    cnx.close()
