VERBOSE = False

inserts_num = 50000
PATHIE = 'micro_censo_escolar_2015/2015/DADOS/{0}'
arquivo = 'ESCOLAS.CSV'
database = 'projetobd_development'
tabela = 'escola'
out_name = tabela+'_inserts/'+tabela+'_inserts_{0}.sql'

arq1 = open(PATHIE.format(arquivo), 'r')

chosen_cols = [1,2,4,5,6,10,11,13,14,15,24,122,120,38]

arq1.readline()

archive_count = 0
insert_count = 0
archive_lines = []

if VERBOSE:
  print('USE {0};\n'.format(database), end='')

for line in arq1:

  cols = line.split('|')
  pick_cols = []

  for col in chosen_cols:

    if cols[col] == '':
      pick_cols.append('null')
    elif cols[col].find('/') != -1:
      date = cols[col].split('/')
      pick_cols.append("'{2}-{1}-{0}'".format(date[0],date[1],date[2]))
    else:
      pick_cols.append("'"+cols[col]+"'")

  colunas = ','.join(pick_cols)

  sql = 'INSERT INTO {0} VALUES ({1});\n'.format(tabela, colunas)
  insert_count += 1
  archive_lines.append(sql)

  if VERBOSE:
    print(sql, end='')

  if insert_count >= inserts_num:
    out = open(out_name.format(archive_count),'w')
    out.write('USE {0};\n'.format(database))
    for insert in archive_lines:
      out.write(insert)
    out.close()
    archive_count += 1
    archive_lines = []
    insert_count = 0

if archive_lines:
  out = open(out_name.format(archive_count),'w')
  out.write('USE {0};\n'.format(database))
  for insert in archive_lines:
    out.write(insert)
  out.close()
  archive_count += 1

print('Generated', archive_count, 'SQL scripts!')
arq1.close()
