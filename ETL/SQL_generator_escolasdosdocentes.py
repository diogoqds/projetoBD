VERBOSE = False

inserts_num = 250000
PATHIE = 'micro_censo_escolar_2015/2015/DADOS/{0}'
regiao = 'SUDESTE'
arquivo = 'DOCENTES_'+regiao+'.CSV'
database = 'projetobd_development'
tabela = 'escolasdosdocentes'
out_name = '{0}_inserts/{0}{1}_inserts_'.format(tabela,regiao)+'{0:{fill}{align}{width}}.sql'
chosen_cols = [118,1]

arq1 = open(PATHIE.format(arquivo), 'r')
arq1.readline()
archive_count = 0
insert_count = 0
archive_lines = []

if VERBOSE:
  print('USE {0};\n'.format(database), end='')

for line in arq1:

  cols = line.split('|')
  pick_cols = []

  for col in chosen_cols:

    if isinstance(col, list):
      pick_cols.append("'{2}-{1:{fill}{align}{width}}-{0:{fill}{align}{width}}'"
        .format(cols[col[0]],cols[col[1]],cols[col[2]], align = '>', fill = '0', width = 2))
    elif cols[col] == '':
      pick_cols.append('null')
    elif cols[col].find('/') != -1:
      date = cols[col].split('/')
      pick_cols.append("'{2}-{1}-{0}'".format(date[0],date[1],date[2]))
    else:
      pick_cols.append("'"+cols[col]+"'")

  colunas = ','.join(pick_cols)

  sql = 'INSERT INTO {0} VALUES ({1});\n'.format(tabela, colunas)
  insert_count += 1
  archive_lines.append(sql)

  if VERBOSE:
    print(sql, end='')

  if insert_count >= inserts_num:
    out_name_generated = out_name.format(archive_count, fill = '0', align = '>', width = 2)
    print('Generating:',out_name_generated)
    out = open(out_name_generated,'w')
    out.write('USE {0};\n'.format(database))
    for insert in archive_lines:
      out.write(insert)
    out.close()
    archive_count += 1
    archive_lines = []
    insert_count = 0

if archive_lines:
  out_name_generated = out_name.format(archive_count, fill = '0', align = '>', width = 2)
  print('Generating:',out_name_generated)
  out = open(out_name_generated,'w')
  out.write('USE {0};\n'.format(database))
  for insert in archive_lines:
    out.write(insert)
  out.close()
  archive_count += 1

print('Generated', archive_count, 'SQL scripts!')
arq1.close()
