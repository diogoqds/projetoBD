Primeira forma normal (1FN):

	Para que um conjunto de tabelas esteja na 1FN, não podem haver atributos multivalorados nas tabelas. Ou seja, as colunas das tabelas devem ser indivisíveis.
	As tabelas de dados iniciais já estavam na 1FN, pois não possuiam atributos multivalorados.

Segunda forma normal (2FN):

	Para que um conjunto de tabelas esteja na 2FN, é preciso que essas tabelas já estejam na 1FN e que cada atributo de uma tabela seja totalmente funcionalmente dependente da(s) chave(s) dessa tabela. Isso implica em duas coisas. Primeiro, para cada chave contida em uma tabela, só existe um valor possível para cada atributo contido nesta mesma tabela. Segundo, todas as chaves contidas em uma tabela são necessárias para se determinar unicamente os atributos dessa tabela. Ou seja, não se pode utilizar menos chaves em uma dada tabela para se chegar a um valor único para os atributos dessa tabela.
	Algumas das tabelas de dados iniciais não estavam na 2FN. Abaixo são listadas quais tabelas continham dados que não podiam ser totalmente funcionalmente determinados pelas chaves das tabelas.

1) Docentes: [PK] idDocentes ¬-> [Atributos] idEscola, idTurma
2) Aluno: [PK] idMatricula ¬-> [Atributos] idTurma

	Percebeu-se que os atributos em questão deveriam ser tratados como chaves, e não atributos. Assim, esses atributos foram retirados das tabelas originais e novas tabelas foram criadas para abrigar esses atributos (agora como chaves), de forma a manter a 2FN. O resultado final foi o seguinte conjunto de operações:

1) (Remoção de atributos) Docentes: [Atributos removidos] idEscola, idTurma
2) (Remoção de atributos) Aluno: [Atributo removido] idTurma
3) (Nova tabela) EscolasDosDocentes: [PK] (idDocente, idEscola)
4) (Nova tabela) TurmasDosDocentes: [PK] (idDocente, idTurma)
5) (Nova tabela) TurmasDosAlunos: [PK] (idMatricula, idTurma)

	Com isso, as tabelas de dados passaram a estar na 2FN.

Terceira forma normal (3FN):

	Para que um conjunto de tabelas esteja na 3FN, é preciso que essas tabelas já estejam na 2FN e que todos os atributos não-chave de cada tabela sejam dependentes não-transitivos das chaves dessa tabela. Ou seja, os atributos não-chave de cada tabela devem ser totalmente funcionalmente dependentes apenas da chave primária dessa tabela. Caso algum atributo seja totalmente funcionalmente dependente de um outro atributo não-chave dessa tabela, a tabela antiga deve ser modificada e uma nova tabela deve ser criada.
	As tabelas de dados iniciais já estavam na 3FN conforme os dados presentes em cada uma. Entretanto, percebemos que um par de dados possivelmente violava essa regra:

1) Escola: [PK] idEscola -> [Atributos] idRegiao, idUF

	Sabemos que cada unidade federativa (UF) pertence exclusivamente a uma região geográfica. Portanto, é lógico supor que: (idUF -> idRegião), o que violaria a 3FN. Entretanto, não encontramos, na tabela de dados UnidadeFederativa fornecida, o atributo idRegiao. Assim, decidimos implementar esses dados manualmente, tendo-se em vista que o Brasil possui apenas 27 UFs e 5 regiões geográficas (Uma quantidade pequena de dados). O resultado final foi o seguinte conjunto de operações:

1) (Adição de atributos) UnidadeFederativa: [Atributo adicionado] idRegiao
2) (Remoção de atributos) Escola: [Atributo removido] idRegiao

Conclusão:

	Com essas modificações, os dados foram normalizados, e, assim, o modelo relacional (MR) pode ser projetado.
