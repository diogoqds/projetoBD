# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do

  create_table "aluno", primary_key: "idMatricula", id: :bigint, unsigned: true, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "CodAluno", null: false, unsigned: true
    t.integer "idEscola", null: false, unsigned: true
    t.date "DataNascAluno", null: false
    t.integer "idSexo", null: false, unsigned: true
    t.integer "idCor_Raca", null: false, unsigned: true
    t.integer "idNacionalidade", null: false, unsigned: true
    t.integer "idPaisOrigem", null: false, unsigned: true
    t.integer "idMunicipioNascimento", unsigned: true
    t.integer "idMunicipioResidencia", unsigned: true
    t.integer "idUFNascimento", unsigned: true
    t.integer "idUFResidencia", unsigned: true
    t.integer "idEtapaEnsino", unsigned: true
    t.integer "idEscolarizacaoSec", unsigned: true
    t.index ["CodAluno"], name: "CodAluno_UNIQUE", unique: true
    t.index ["idCor_Raca"], name: "fk_Aluno_Cor_Raça1_idx"
    t.index ["idEscola"], name: "fk_Aluno_Escola1_idx"
    t.index ["idEscolarizacaoSec"], name: "fk_Aluno_EscolarizacaoSec1_idx"
    t.index ["idEtapaEnsino"], name: "fk_Aluno_EtapaEnsino1_idx"
    t.index ["idMatricula"], name: "idMatricula_UNIQUE", unique: true
    t.index ["idMunicipioNascimento"], name: "fk_Aluno_Municipio1_idx"
    t.index ["idMunicipioResidencia"], name: "fk_Aluno_Municipio2_idx"
    t.index ["idNacionalidade"], name: "fk_Aluno_Nacionalidade1_idx"
    t.index ["idPaisOrigem"], name: "fk_Aluno_PaisOrigem1_idx"
    t.index ["idSexo"], name: "fk_Aluno_Sexo1_idx"
    t.index ["idUFNascimento"], name: "fk_Aluno_UnidadeFederativa1_idx"
    t.index ["idUFResidencia"], name: "fk_Aluno_UnidadeFederativa2_idx"
  end

  create_table "backupdocentes", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "idBackupDocentes", null: false, unsigned: true
    t.date "DataNascDocente", null: false
    t.integer "idSexo", null: false, unsigned: true
    t.integer "idCor_Raca", null: false, unsigned: true
    t.integer "idNacionalidade", null: false, unsigned: true
    t.integer "idPaisOrigem", null: false, unsigned: true
    t.integer "idMunicipioNascimento", null: false, unsigned: true
    t.integer "idMunicipioResidencia", null: false, unsigned: true
    t.integer "idUFNascimento", null: false, unsigned: true
    t.integer "idUFResidencia", null: false, unsigned: true
    t.integer "Deficiencia", limit: 1, null: false
    t.integer "Def_Cegueira", limit: 1, default: 0
    t.integer "Def_BaixaVisao", limit: 1, default: 0
    t.integer "Def_Surdez", limit: 1, default: 0
    t.integer "Def_DefAuditiva", limit: 1, default: 0
    t.integer "Def_SurdoCegueira", limit: 1, default: 0
    t.integer "Def_DefFisica", limit: 1
    t.integer "Def_DefIntelectual", limit: 1
    t.integer "Def_DefMultipla", limit: 1
    t.integer "idEscolaridade", null: false, unsigned: true
    t.integer "idSituacaoFuncional", null: false, unsigned: true
  end

  create_table "backupescola", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "idBackupEscola", null: false, unsigned: true
    t.string "NomeEscola", limit: 100, null: false
    t.integer "idSitEscola", null: false, unsigned: true
    t.date "InicioAnoLetivo"
    t.date "FimAnoLetivo"
    t.integer "id_UF", null: false, unsigned: true
    t.integer "idMunicipio", null: false, unsigned: true
    t.integer "idDependenciaAdm", null: false, unsigned: true
    t.integer "idTipoLocalizacao", null: false, unsigned: true
    t.integer "idCatPrivada", unsigned: true
    t.integer "CodIES", unsigned: true
    t.integer "TotalFuncionarios", unsigned: true
    t.integer "Internet", limit: 1
    t.integer "Compartilhada", limit: 1
  end

  create_table "categoriaprivada", primary_key: "idCatPrivada", id: :integer, unsigned: true, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "NomeCatPrivada", limit: 20, null: false
    t.index ["NomeCatPrivada"], name: "NomeCatPrivada_UNIQUE", unique: true
    t.index ["idCatPrivada"], name: "idCategoriaPrivada_UNIQUE", unique: true
  end

  create_table "cor_raca", primary_key: "idCor_Raca", id: :integer, unsigned: true, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "TipoCor_Raca", limit: 15, null: false
    t.index ["TipoCor_Raca"], name: "NomeCor_Raça_UNIQUE", unique: true
    t.index ["idCor_Raca"], name: "idCor_Raça_UNIQUE", unique: true
  end

  create_table "dependenciaadm", primary_key: "idDependenciaAdm", id: :integer, unsigned: true, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "TipoDependenciaAdm", limit: 10, null: false
    t.index ["TipoDependenciaAdm"], name: "NomeDependenciaAdm_UNIQUE", unique: true
    t.index ["idDependenciaAdm"], name: "idDependenciaAdm_UNIQUE", unique: true
  end

  create_table "docente", primary_key: "idDocente", id: :bigint, unsigned: true, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.date "DataNascDocente", null: false
    t.integer "idSexo", null: false, unsigned: true
    t.integer "idCor_Raca", null: false, unsigned: true
    t.integer "idNacionalidade", null: false, unsigned: true
    t.integer "idPaisOrigem", null: false, unsigned: true
    t.integer "idMunicipioNascimento", unsigned: true
    t.integer "idMunicipioResidencia", unsigned: true
    t.integer "idUFNascimento", unsigned: true
    t.integer "idUFResidencia", unsigned: true
    t.integer "Deficiencia", limit: 1, null: false
    t.integer "Def_Cegueira", limit: 1, default: 0
    t.integer "Def_BaixaVisao", limit: 1, default: 0
    t.integer "Def_Surdez", limit: 1, default: 0
    t.integer "Def_DefAuditiva", limit: 1, default: 0
    t.integer "Def_SurdoCegueira", limit: 1, default: 0
    t.integer "Def_DefFisica", limit: 1
    t.integer "Def_DefIntelectual", limit: 1
    t.integer "Def_DefMultipla", limit: 1
    t.integer "idEscolaridade", null: false, unsigned: true
    t.integer "idSituacaoFuncional", unsigned: true
    t.index ["idCor_Raca"], name: "fk_Docente_Cor_Raça1_idx"
    t.index ["idDocente"], name: "idDocente_UNIQUE", unique: true
    t.index ["idEscolaridade"], name: "fk_Docente_Escolaridade1_idx"
    t.index ["idMunicipioNascimento"], name: "fk_Docente_Municipio1_idx"
    t.index ["idMunicipioResidencia"], name: "fk_Docente_Municipio2_idx"
    t.index ["idNacionalidade"], name: "fk_Docente_Nacionalidade1_idx"
    t.index ["idPaisOrigem"], name: "fk_Docente_PaisOrigem1_idx"
    t.index ["idSexo"], name: "fk_Docente_Sexo1_idx"
    t.index ["idSituacaoFuncional"], name: "fk_Docente_SituacaoFuncional1_idx"
    t.index ["idUFNascimento"], name: "fk_Docente_UnidadeFederativa1_idx"
    t.index ["idUFResidencia"], name: "fk_Docente_UnidadeFederativa2_idx"
  end

  create_table "escola", primary_key: "idEscola", id: :integer, unsigned: true, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "NomeEscola", limit: 100, null: false
    t.integer "idSitEscola", null: false, unsigned: true
    t.date "InicioAnoLetivo"
    t.date "FimAnoLetivo"
    t.integer "id_UF", null: false, unsigned: true
    t.integer "idMunicipio", null: false, unsigned: true
    t.integer "idDependenciaAdm", null: false, unsigned: true
    t.integer "idTipoLocalizacao", null: false, unsigned: true
    t.integer "idCatPrivada", unsigned: true
    t.integer "CodIES", unsigned: true
    t.integer "TotalFuncionarios", unsigned: true
    t.integer "Internet", limit: 1
    t.integer "Compartilhada", limit: 1
    t.index ["idCatPrivada"], name: "fk_Escola_CategoriaPrivada1_idx"
    t.index ["idDependenciaAdm"], name: "fk_Escola_DependenciaAdm1_idx"
    t.index ["idEscola"], name: "IDEscola_UNIQUE", unique: true
    t.index ["idMunicipio"], name: "fk_Escola_Municipio1_idx"
    t.index ["idSitEscola"], name: "fk_Escola_SituacaoEscola_idx"
    t.index ["idTipoLocalizacao"], name: "fk_Escola_TipoLocalizacao1_idx"
    t.index ["id_UF"], name: "fk_Escola_UnidadeFederativa1_idx"
  end

  create_table "escolaridade", primary_key: "idEscolaridade", id: :integer, unsigned: true, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "GrauEscolaridade", limit: 25, null: false
    t.index ["GrauEscolaridade"], name: "GrauEscolaridade_UNIQUE", unique: true
    t.index ["idEscolaridade"], name: "idEscolaridade_UNIQUE", unique: true
  end

  create_table "escolarizacaosec", primary_key: "idEscolarizacaoSec", id: :integer, unsigned: true, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "LocalEscolarizacaoSec", limit: 15, null: false
    t.index ["LocalEscolarizacaoSec"], name: "LocalEscolarizacaoSec_UNIQUE", unique: true
    t.index ["idEscolarizacaoSec"], name: "idEscolarizacaoSec_UNIQUE", unique: true
  end

  create_table "escolasdosdocentes", primary_key: ["idEscola", "idDocente"], force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "idEscola", null: false, unsigned: true
    t.bigint "idDocente", null: false, unsigned: true
    t.index ["idDocente"], name: "fk_Escola_has_Docente_Docente1_idx"
    t.index ["idEscola"], name: "fk_Escola_has_Docente_Escola1_idx"
  end

  create_table "etapaensino", primary_key: "idEtapaEnsino", id: :integer, unsigned: true, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "NomeEtapaEnsino", limit: 120, null: false
    t.index ["NomeEtapaEnsino"], name: "NomeEtapaEnsino_UNIQUE", unique: true
    t.index ["idEtapaEnsino"], name: "idEtapaEnsino_UNIQUE", unique: true
  end

  create_table "funcaoescolar", primary_key: "idFuncaoEscolar", id: :integer, unsigned: true, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "NomeFuncaoEscolar", limit: 70, null: false
    t.index ["NomeFuncaoEscolar"], name: "NomeFuncaoEscolar_UNIQUE", unique: true
    t.index ["idFuncaoEscolar"], name: "idFuncaoEscolar_UNIQUE", unique: true
  end

  create_table "funcoesdosdocentes", primary_key: ["idDocente", "idFuncaoEscolar"], force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "idDocente", null: false, unsigned: true
    t.integer "idFuncaoEscolar", null: false, unsigned: true
    t.index ["idDocente"], name: "fk_Docente_has_FuncaoEscolar_Docente1_idx"
    t.index ["idFuncaoEscolar"], name: "fk_Docente_has_FuncaoEscolar_FuncaoEscolar1_idx"
  end

  create_table "mediacaodidped", primary_key: "idMediacaoDidPed", id: :integer, unsigned: true, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "NomeMediacaoDidPed", limit: 30, null: false
    t.index ["NomeMediacaoDidPed"], name: "NomeMediacaoDidPedcol_UNIQUE", unique: true
    t.index ["idMediacaoDidPed"], name: "idMediacaoDidPed_UNIQUE", unique: true
  end

  create_table "municipio", primary_key: "idMunicipio", id: :integer, unsigned: true, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "NomeMunicipio", limit: 100, null: false
    t.index ["idMunicipio"], name: "IDMunicipio_UNIQUE", unique: true
  end

  create_table "nacionalidade", primary_key: "idNacionalidade", id: :integer, unsigned: true, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "TipoNacionalidade", limit: 45, null: false
    t.index ["TipoNacionalidade"], name: "TipoNacionalidade_UNIQUE", unique: true
    t.index ["idNacionalidade"], name: "idNacionalidade_UNIQUE", unique: true
  end

  create_table "paisorigem", primary_key: "idPaisOrigem", id: :integer, unsigned: true, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "NomePaisOrigem", limit: 50, null: false
    t.string "SiglaPaisOrigem", limit: 3
    t.index ["NomePaisOrigem"], name: "NomePaisOrigem_UNIQUE", unique: true
    t.index ["idPaisOrigem"], name: "idPaisOrigem_UNIQUE", unique: true
  end

  create_table "regiao", primary_key: "idRegiao", id: :integer, unsigned: true, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "NomeRegiaoGeo", limit: 15, null: false
    t.index ["NomeRegiaoGeo"], name: "NomeRegiaoGeo_UNIQUE", unique: true
    t.index ["idRegiao"], name: "IDRegiao_UNIQUE", unique: true
  end

  create_table "sexo", primary_key: "idSexo", id: :integer, unsigned: true, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "TipoSexo", limit: 9, null: false
    t.index ["TipoSexo"], name: "TipoSexo_UNIQUE", unique: true
    t.index ["idSexo"], name: "idSexo_UNIQUE", unique: true
  end

  create_table "situacaoescola", primary_key: "idSitEscola", id: :integer, unsigned: true, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "NomeSitEscola", limit: 40, null: false
    t.index ["NomeSitEscola"], name: "NomeSitEscola_UNIQUE", unique: true
    t.index ["idSitEscola"], name: "IDSitEscola_UNIQUE", unique: true
  end

  create_table "situacaofuncional", primary_key: "idSituacaoFuncional", id: :integer, unsigned: true, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "TipoSituacaoFuncional", limit: 35, null: false
    t.index ["TipoSituacaoFuncional"], name: "NomeSituacaoFuncional_UNIQUE", unique: true
    t.index ["idSituacaoFuncional"], name: "idSituacaoFuncional_UNIQUE", unique: true
  end

  create_table "tipolocalizacao", primary_key: "idTipoLocalizacao", id: :integer, unsigned: true, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "NomeTpLocalizacao", limit: 6, null: false
    t.index ["NomeTpLocalizacao"], name: "NomeTpLocalizacao_UNIQUE", unique: true
    t.index ["idTipoLocalizacao"], name: "idTipoLocalizacao_UNIQUE", unique: true
  end

  create_table "turma", primary_key: "idTurma", id: :integer, unsigned: true, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "idEscola", null: false, unsigned: true
    t.string "NomeTurma", limit: 80, null: false
    t.string "HoraInicial", limit: 2
    t.integer "DuracaoMin", unsigned: true
    t.integer "NumMatriculas", unsigned: true
    t.integer "idMediacaoDidPed", null: false, unsigned: true
    t.integer "ClasseEspecial", limit: 1
    t.integer "idEtapaEnsino", unsigned: true
    t.integer "DiasPorSemana", unsigned: true
    t.integer "Dia_Domingo", limit: 1
    t.integer "Dia_Segunda", limit: 1
    t.integer "Dia_Terça", limit: 1
    t.integer "Dia_Quarta", limit: 1
    t.integer "Dia_Quinta", limit: 1
    t.integer "Dia_Sexta", limit: 1
    t.integer "Dia_Sabado", limit: 1
    t.index ["idEscola"], name: "fk_Turma_Escola1_idx"
    t.index ["idEtapaEnsino"], name: "fk_Turma_EtapaEnsino1_idx"
    t.index ["idMediacaoDidPed"], name: "fk_Turma_MediacaoDidPed1_idx"
    t.index ["idTurma"], name: "idTurma_UNIQUE", unique: true
  end

  create_table "turmasdosalunos", primary_key: ["idTurma", "idMatricula"], force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "idTurma", null: false, unsigned: true
    t.bigint "idMatricula", null: false, unsigned: true
    t.index ["idMatricula"], name: "fk_Turma_has_Matricula_Matricula1_idx"
    t.index ["idTurma"], name: "fk_Turma_has_Matricula_Turma1_idx"
  end

  create_table "turmasdosdocentes", primary_key: ["idTurma", "idDocente"], force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "idTurma", null: false, unsigned: true
    t.bigint "idDocente", null: false, unsigned: true
    t.index ["idDocente"], name: "fk_Turma_has_Docente_Docente1_idx"
    t.index ["idTurma"], name: "fk_Turma_has_Docente_Turma1_idx"
  end

  create_table "unidadefederativa", primary_key: "idUF", id: :integer, unsigned: true, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "NomeUF", limit: 25, null: false
    t.integer "idRegiao", null: false, unsigned: true
    t.index ["NomeUF"], name: "NomeUF_UNIQUE", unique: true
    t.index ["idRegiao"], name: "fk_UnidadeFederativa_Regiao1_idx"
    t.index ["idUF"], name: "ID_UF_UNIQUE", unique: true
  end

  add_foreign_key "aluno", "cor_raca", column: "idCor_Raca", primary_key: "idCor_Raca", name: "fk_Aluno_Cor_Raça1"
  add_foreign_key "aluno", "escola", column: "idEscola", primary_key: "idEscola", name: "fk_Aluno_Escola1"
  add_foreign_key "aluno", "escolarizacaosec", column: "idEscolarizacaoSec", primary_key: "idEscolarizacaoSec", name: "fk_Aluno_EscolarizacaoSec1"
  add_foreign_key "aluno", "etapaensino", column: "idEtapaEnsino", primary_key: "idEtapaEnsino", name: "fk_Aluno_EtapaEnsino1"
  add_foreign_key "aluno", "municipio", column: "idMunicipioNascimento", primary_key: "idMunicipio", name: "fk_Aluno_Municipio1"
  add_foreign_key "aluno", "municipio", column: "idMunicipioResidencia", primary_key: "idMunicipio", name: "fk_Aluno_Municipio2"
  add_foreign_key "aluno", "nacionalidade", column: "idNacionalidade", primary_key: "idNacionalidade", name: "fk_Aluno_Nacionalidade1"
  add_foreign_key "aluno", "paisorigem", column: "idPaisOrigem", primary_key: "idPaisOrigem", name: "fk_Aluno_PaisOrigem1"
  add_foreign_key "aluno", "sexo", column: "idSexo", primary_key: "idSexo", name: "fk_Aluno_Sexo1"
  add_foreign_key "aluno", "unidadefederativa", column: "idUFNascimento", primary_key: "idUF", name: "fk_Aluno_UnidadeFederativa1"
  add_foreign_key "aluno", "unidadefederativa", column: "idUFResidencia", primary_key: "idUF", name: "fk_Aluno_UnidadeFederativa2"
  add_foreign_key "docente", "cor_raca", column: "idCor_Raca", primary_key: "idCor_Raca", name: "fk_Docente_Cor_Raça1"
  add_foreign_key "docente", "escolaridade", column: "idEscolaridade", primary_key: "idEscolaridade", name: "fk_Docente_Escolaridade1"
  add_foreign_key "docente", "municipio", column: "idMunicipioNascimento", primary_key: "idMunicipio", name: "fk_Docente_Municipio1"
  add_foreign_key "docente", "municipio", column: "idMunicipioResidencia", primary_key: "idMunicipio", name: "fk_Docente_Municipio2"
  add_foreign_key "docente", "nacionalidade", column: "idNacionalidade", primary_key: "idNacionalidade", name: "fk_Docente_Nacionalidade1"
  add_foreign_key "docente", "paisorigem", column: "idPaisOrigem", primary_key: "idPaisOrigem", name: "fk_Docente_PaisOrigem1"
  add_foreign_key "docente", "sexo", column: "idSexo", primary_key: "idSexo", name: "fk_Docente_Sexo1"
  add_foreign_key "docente", "unidadefederativa", column: "idUFNascimento", primary_key: "idUF", name: "fk_Docente_UnidadeFederativa1"
  add_foreign_key "docente", "unidadefederativa", column: "idUFResidencia", primary_key: "idUF", name: "fk_Docente_UnidadeFederativa2"
  add_foreign_key "escola", "categoriaprivada", column: "idCatPrivada", primary_key: "idCatPrivada", name: "fk_Escola_CategoriaPrivada1"
  add_foreign_key "escola", "dependenciaadm", column: "idDependenciaAdm", primary_key: "idDependenciaAdm", name: "fk_Escola_DependenciaAdm1"
  add_foreign_key "escola", "municipio", column: "idMunicipio", primary_key: "idMunicipio", name: "fk_Escola_Municipio1"
  add_foreign_key "escola", "situacaoescola", column: "idSitEscola", primary_key: "idSitEscola", name: "fk_Escola_SituacaoEscola"
  add_foreign_key "escola", "tipolocalizacao", column: "idTipoLocalizacao", primary_key: "idTipoLocalizacao", name: "fk_Escola_TipoLocalizacao1"
  add_foreign_key "escola", "unidadefederativa", column: "id_UF", primary_key: "idUF", name: "fk_Escola_UnidadeFederativa1"
  add_foreign_key "escolasdosdocentes", "docente", column: "idDocente", primary_key: "idDocente", name: "fk_Escola_has_Docente_Docente1"
  add_foreign_key "escolasdosdocentes", "escola", column: "idEscola", primary_key: "idEscola", name: "fk_Escola_has_Docente_Escola1"
  add_foreign_key "funcoesdosdocentes", "docente", column: "idDocente", primary_key: "idDocente", name: "fk_Docente_has_FuncaoEscolar_Docente1"
  add_foreign_key "funcoesdosdocentes", "funcaoescolar", column: "idFuncaoEscolar", primary_key: "idFuncaoEscolar", name: "fk_Docente_has_FuncaoEscolar_FuncaoEscolar1"
  add_foreign_key "turma", "escola", column: "idEscola", primary_key: "idEscola", name: "fk_Turma_Escola1", on_update: :cascade, on_delete: :cascade
  add_foreign_key "turma", "etapaensino", column: "idEtapaEnsino", primary_key: "idEtapaEnsino", name: "fk_Turma_EtapaEnsino1"
  add_foreign_key "turma", "mediacaodidped", column: "idMediacaoDidPed", primary_key: "idMediacaoDidPed", name: "fk_Turma_MediacaoDidPed1"
  add_foreign_key "turmasdosalunos", "aluno", column: "idMatricula", primary_key: "idMatricula", name: "fk_Turma_has_Matricula_Matricula1"
  add_foreign_key "turmasdosalunos", "turma", column: "idTurma", primary_key: "idTurma", name: "fk_Turma_has_Matricula_Turma1"
  add_foreign_key "turmasdosdocentes", "docente", column: "idDocente", primary_key: "idDocente", name: "fk_Turma_has_Docente_Docente1"
  add_foreign_key "turmasdosdocentes", "turma", column: "idTurma", primary_key: "idTurma", name: "fk_Turma_has_Docente_Turma1"
  add_foreign_key "unidadefederativa", "regiao", column: "idRegiao", primary_key: "idRegiao", name: "fk_UnidadeFederativa_Regiao1"
end
