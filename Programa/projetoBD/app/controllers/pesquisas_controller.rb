class PesquisasController < ApplicationController
  def index
    @cards = [
      ['Funcionários',
      'Quantas escolas possuem mais de 50 funcionários?',
      funcionarios_path(1)],
      ['Aulas na semana',
      'Quantas turmas dão aula 5 dias ou mais na semana?',
      quant_aulas_path(1)],
      ['Aulas no final de semana',
      'Quantas turmas dão aula sábado ou domingo?',
      aulas_fds_path(1)],
      ['Escolaridade secundária',
      'Como está distribuída a escolarização secundária',
      escolaridade_secundaria_path(1)],
      ['Docentes nas áreas rurais e urbanas',
      'Como está distribuído os docentes por área urbana e rural?',
      formacao_ur_path(1)],
      ['Acesso a internet',
      'Quantas escolas possuem acesso a internet?',
      escolas_path(1)]
    ]

  end

  def chart
    @estats_tabelas1 = {
      'Escolas' => Escola.count,
      'Turmas' => Turma.count,
      'Alunos' => Aluno.count,
      'Docentes' => Docente.count
    }
    render json: @estats_tabelas1
  end
end
