class EscolasController < ApplicationController
  def index
    @pergunta = 'Quantas escolas possuem acesso a internet?'
    @query = Escola.where(Internet: 1)
    .limit(20).offset(20*(params[:page].to_i-1))
  end
  def chart
    @graph = {
      'Tem' => Escola.where(Internet: 1).count,
      'Não tem' => Escola.where(Internet: 0).count,
      'Dados não disponibilizados' => Escola.where(Internet: nil).count
    }
    render json: @graph
  end
end
