class FormacaoUrController < ApplicationController
  def index
    @pergunta = 'Como está distribuído os docentes por área urbana e rural?'
    @base = Docente
    .joins("INNER JOIN escolasdosdocentes
      ON docente.idDocente = escolasdosdocentes.idDocente")
    .joins("INNER JOIN escola
      ON escola.idEscola = escolasdosdocentes.idEscola")
    .joins("INNER JOIN tipolocalizacao
      ON escola.idTipoLocalizacao = tipolocalizacao.idTipoLocalizacao")
    @query = @base.select(:idDocente, :NomeTpLocalizacao)
    .limit(20).offset(20*(params[:page].to_i-1))
  end

  def chart
    @base = Docente
    .joins("INNER JOIN escolasdosdocentes
      ON docente.idDocente = escolasdosdocentes.idDocente")
    .joins("INNER JOIN escola
      ON escola.idEscola = escolasdosdocentes.idEscola")
    .joins("INNER JOIN tipolocalizacao
      ON escola.idTipoLocalizacao = tipolocalizacao.idTipoLocalizacao")
    @graph = @base.group(:NomeTpLocalizacao).count
    render json: @graph
  end
end
