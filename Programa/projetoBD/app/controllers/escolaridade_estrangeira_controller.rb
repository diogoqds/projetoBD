class EscolaridadeEstrangeiraController < ApplicationController
  def index
   @pergunta = 'Como está distribuída a escolarização secundária'

   @query = Aluno
   .joins('INNER JOIN escolarizacaosec
    ON aluno.idEscolarizacaoSec = escolarizacaosec.idEscolarizacaoSec')
   .select(:idMatricula, 'escolarizacaosec.LocalEscolarizacaoSec')
   .limit(20).offset(20*(params[:page].to_i-1))
  end

  def chart
    @graph = Aluno
   .joins('INNER JOIN escolarizacaosec
    ON aluno.idEscolarizacaoSec = escolarizacaosec.idEscolarizacaoSec')
   .group('LocalEscolarizacaoSec').count

   render json: @graph
 end

end
