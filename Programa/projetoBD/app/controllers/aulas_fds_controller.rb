class AulasFdsController < ApplicationController
  def index
    @pergunta = 'Quantas turmas dão aula sábado ou domingo?'
    @query = Turma.select(:idTurma, :Dia_Sabado, :Dia_Domingo)
    .where('Dia_Domingo > 0 OR Dia_Sabado > 0')
    .limit(20).offset(20*(params[:page].to_i-1))
  end

  def chart
    @graph = {'Sabado' => Turma.where('Dia_Sabado > 0 AND Dia_Domingo = 0').count,
      'Domingo' => Turma.where('Dia_Domingo > 0 AND Dia_Sabado = 0').count,
      'Sábado e Domingo' => Turma.where('Dia_Domingo > 0 AND Dia_Sabado > 0').count,
      'Só durante a semana' => Turma.where('Dia_Sabado = 0 AND Dia_Domingo = 0').count}
    render json: @graph
  end
end
