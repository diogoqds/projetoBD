class FuncionariosController < ApplicationController
  def index
    @pergunta = 'Quantas escolas possuem mais de 50 funcionários?'
    @query = Escola.select(:idEscola, :NomeEscola, :TotalFuncionarios).where('`escola`.`TotalFuncionarios` > 50').order(:TotalFuncionarios)
    .limit(20).offset(20*(params[:page].to_i-1))
  end

  def chart
    @graph = {'Mais de 50' => Escola.where('TotalFuncionarios > 50').count,
      '50 ou menos' => Escola.where('TotalFuncionarios <= 50').count,
      'Não disponibilizado' => Escola.where('TotalFuncionarios IS NULL').count}
    render json: @graph
  end

end
