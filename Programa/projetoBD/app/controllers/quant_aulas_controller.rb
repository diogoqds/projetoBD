class QuantAulasController < ApplicationController
  def index
    @pergunta = 'Quantas turmas dão aula 5 dias ou mais na semana?'
    @query = Turma.select(:idTurma, :DiasPorSemana).where('`turma`.`DiasPorSemana` >= 5').order(:DiasPorSemana).limit(20).offset(20*(params[:page].to_i-1))
  end

  def chart
    @graph = {'5 ou mais' => Turma.where('DiasPorSemana >= 5').count,
      'Menos de 5' => Turma.where('DiasPorSemana < 5').count,
      'Não disponibilizado' => Turma.where('DiasPorSemana IS NULL').count}
    render json: @graph
  end
end
