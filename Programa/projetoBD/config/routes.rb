Rails.application.routes.draw do

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'pesquisas#index'

  get 'aulas_fds/chart', to: 'aulas_fds#chart', as: 'aulas_fds_chart'
  get 'aulas_fds/:page', to: 'aulas_fds#index', as: 'aulas_fds'
  get 'escolaridade_secundaria/chart', to: 'escolaridade_secundaria#chart', as: 'escolaridade_secundaria_chart'
  get 'escolaridade_secundaria/:page', to: 'escolaridade_secundaria#index', as: 'escolaridade_secundaria'
  get 'escolas/chart', to: 'escolas#chart', as: 'escolas_chart'
  get 'escolas/:page', to: 'escolas#index', as: 'escolas'
  get 'formacao_ur/chart', to: 'formacao_ur#chart', as: 'formacao_ur_chart'
  get 'formacao_ur/:page', to: 'formacao_ur#index', as: 'formacao_ur'
  get 'funcionarios/chart', to: 'funcionarios#chart', as: 'funcionarios_chart'
  get 'funcionarios/:page', to: 'funcionarios#index', as: 'funcionarios'
  get 'pesquisas/chart', to: 'pesquisas#chart', as: 'pesquisas_chart'
  get 'pesquisas', to: 'pesquisas#index', as: 'pesquisas'
  get 'quant_aulas/chart', to: 'quant_aulas#chart', as: 'quant_aulas_chart'
  get 'quant_aulas/:page', to: 'quant_aulas#index', as: 'quant_aulas'

end
