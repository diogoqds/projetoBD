rails generate model Aluno

rails generate model Categoriaprivada NomeCatPrivada:string

rails generate model CorRaca

rails generate model Dependenciaadm TipoDependenciaAdm:string

rails generate model Docente

rails generate model Escola NomeEscola:string idSitEscola:integer InicioAnoLetivo:date FimAnoLetivo:date id_UF:integer idMunicipio:integer idDependenciaAdm:integer idTipoLocalizacao:integer idCatPrivada:integer idEscolaSede:integer CodIES:integer TotalFuncionarios:integer Internet:integer Compartilhada:integer

rails generate model Escolaridade GrauEscolaridade:string

rails generate model Escolarizacaosec LocalEscolarizacaoSec:string

rails generate model Escolasdosdocente idEscola:integer idDocente:integer

rails generate model Etapaensino NomeEtapaEnsino:string

rails generate model Funcaoescolar NomeFuncaoEscolar:string

rails generate model Mediacaodidped NomeMediacaoDidPed:string

rails generate model Municipio NomeMunicipio:string

rails generate model Nacionalidade TipoNacionalidade:string

rails generate model Paisorigem NomePaisOrigem:string SiglaPaisOrigem:string

rails generate model Regiao NomeRegiaoGeo:string

rails generate model Sexo TipoSexo:string

rails generate model Situacaoescola NomeSitEscola:string

rails generate model Situacaofuncional TipoSituacaoFuncional:string

rails generate model Tipolocalizacao NomeTpLocalizacao:string

rails generate model Turma

rails generate model Turmasdosaluno idTurma:integer idMatricula:integer

rails generate model Turmasdosdocente idTurma:integer idDocente:integer

rails generate model Unidadefederativa NomeUF:string idRegiao:integer
